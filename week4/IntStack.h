/* 
 * File:   IntStack.h
 * Author: lasse
 *
 * Created on November 19, 2012, 7:19 PM
 */

#ifndef INTSTACK_H
#define	INTSTACK_H

class IntStack {
public:
    explicit IntStack(size_t sz = 90);
    IntStack(const IntStack&);
    virtual ~IntStack();
    void push(int);
    int pop();
    bool is_empty();
    IntStack& operator=(const IntStack&);
private:
    unsigned size_;
    unsigned top_;
    int* array_;

};

#endif	/* INTSTACK_H */

