/* 
 * File:   IntStack.cpp
 * Author: lasse
 * 
 * Created on November 19, 2012, 7:19 PM
 */

#include <stdexcept>

#include "IntStack.h"

IntStack::IntStack(unsigned sz) : size_(sz), top_(0), array_(new int[sz]) {
}

IntStack::IntStack(const IntStack& stack) :
size_(stack.size_), top_(stack.top_), array_(new int[size_]) {
    for (int i = 0; i < top_; ++i) {
        array_[i] = stack. array_[i];
    }
}

IntStack::~IntStack() {
    delete[] array_;
}

void IntStack::push(int i) {
    if (top_ == size_)
        throw std::logic_error("stack overflow");
    array_[top_++] = i;
}

int IntStack::pop(){
    if(top_ == 0){
        throw std::logic_error("stack empty");
    }
    return array_[--top_];
}

bool IntStack::is_empty(){
    return top_ == 0;
}

IntStack& IntStack::operator=(const IntStack& stack){
    size_ = stack.size_;
    top_ = stack.top_;
    for (int i = 0; i < top_; i++) {
        array_[i] = stack.array_[i];
    }
    return *this;
}

