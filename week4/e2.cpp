/* 
 * File:   e2.cpp
 * Author: lasse
 *
 * Created on November 16, 2012, 3:30 PM
 */

#include <cstdlib>
#include <iostream>

#include "my_complex.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    my_complex a = 3;
    const my_complex b(2, 2);
    my_complex c(-4, -5);
    my_complex d(0, 2);
    cout << a << endl;
    cout << b + c << endl;
    cout << d << endl;

//    cout << "input:" << endl;
//    my_complex z;
//    cin >> z;
//    cout << z << endl;

    cout << endl << "Operation tests:" << endl;
    my_complex z1(2, 4);
    my_complex z2(3, 3);
    cout << "(" << z1 << ") + (" << z2 << ") = " << z1 + z2 << endl; 
    cout << "(" << z1 << ") - (" << z2 << ") = " << z1 - z2 << endl; 
    cout << "(" << z1 << ") * (" << z2 << ") = " << z1 * z2 << endl; 
    cout << "(" << z1 << ") / (" << z2 << ") = " << z1 / z2 << endl; 
    return 0;
}

