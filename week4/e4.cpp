/* 
 * File:   e4.cpp
 * Author: lasse
 *
 * Created on November 20, 2012, 11:04 AM
 */

#include <cstdlib>
#include <iostream>

#include "IntStack.h"

using namespace std;

int main(int argc, char** argv) {
    IntStack stack;
    for (int i = 0; i < 10; i++) {
        stack.push(i);
    }
    while(!stack.is_empty()) {
        cout << stack.pop() << " ";
    }
    cout << endl;
    return 0;
}

