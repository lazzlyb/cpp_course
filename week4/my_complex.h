/* 
 * File:   my_complex.h
 * Author: lasse
 *
 * Created on November 16, 2012, 3:13 PM
 */

#ifndef MY_COMPLEX_H
#define	MY_COMPLEX_H

#include <iostream>

class my_complex {
public:
    my_complex();
    my_complex(const my_complex&);
    my_complex(double);
    my_complex(double, double);
    my_complex operator+() const;
    my_complex operator-() const;
    my_complex operator+(const my_complex&) const;
    my_complex operator-(const my_complex&) const;
    my_complex operator*(const my_complex&) const;
    my_complex operator/(const my_complex&) const;
    virtual ~my_complex();
    double re;
    double im;
private:

};

my_complex conj(const my_complex&);
my_complex inv(const my_complex&);
std::ostream& operator<<(std::ostream&, const my_complex&);
std::istream& operator>>(std::istream&, my_complex&);

#endif	/* MY_COMPLEX_H */

