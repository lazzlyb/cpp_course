/* 
 * File:   my_complex.cpp
 * Author: lasse
 * 
 * Created on November 16, 2012, 3:13 PM
 */

#include "my_complex.h"

my_complex::my_complex() : re(0), im(0) {
}

my_complex::my_complex(const my_complex& orig) : re(orig.re), im(orig.im) {
}

my_complex::my_complex(double x) : re(x), im(0) {
}

my_complex::my_complex(double r, double i) : re(r), im(i) {
}

my_complex::~my_complex() {
}

my_complex my_complex::operator+() const {
    return my_complex(re, im);
}

my_complex my_complex::operator-() const {
    return my_complex(-re, -im);
}

my_complex my_complex::operator+(const my_complex& z) const {
    return my_complex(re + z.re, im + z.im);
}

my_complex my_complex::operator-(const my_complex& z) const {
    return *this +(-z);
}

my_complex my_complex::operator*(const my_complex& z) const {
    return my_complex(re * z.re - im * z.im, im * z.re + re * z.im);
}

my_complex my_complex::operator/(const my_complex& z) const {
    return *this * inv(z);
}

std::ostream& operator<<(std::ostream& io, const my_complex& z) {
    if (z.re == 0 && z.im == 0) {
        io << 0;
    } else if (z.re == 0) {
        io << z.im << "i";
    } else if (z.im == 0) {
        io << z.re;
    } else {
        io << z.re;
        if (z.im > 0) {
            io << " + ";
            if (z.im != 1)
                io << z.im;
        } else if (z.im < 0)
            io << " - " << -z.im;
        io << "i";
    }
    return io;
}

std::istream& operator>>(std::istream& io, my_complex& z) {
    io >> z.re;
    io >> z.im;
    return io;
}

my_complex conj(const my_complex& z) {
    return my_complex(z.re, -z.im);
}

my_complex inv(const my_complex& z) {
    double denum = z.re * z.re + z.im * z.im;
    return my_complex(z.re / denum, -z.im / denum);
}

