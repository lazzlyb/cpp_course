
#include <cstdlib>

#include "my_iterator.h"

my_iterator::my_iterator() : data(NULL) {
}

my_iterator::my_iterator(const my_iterator& orig) : data(orig.data) {
}

my_iterator::my_iterator(char* dptr) : data(dptr) {
}

my_iterator::~my_iterator() {
}

bool my_iterator::operator==(const my_iterator& it) const {
    return data == it.data;
}

bool my_iterator::operator!=(const my_iterator& it) const {
    return !(*this == it);
}

my_iterator& my_iterator::operator++() {
    ++data;
    return *this;
}

my_iterator my_iterator::operator++(int) {
    my_iterator temp(*this);
    ++data;
    return temp;
}

my_iterator& my_iterator::operator--() {
    --data;
    return *this;
}

my_iterator my_iterator::operator--(int) {
    my_iterator temp(*this);
    --data;
    return temp;
}

my_iterator& my_iterator::operator=(const my_iterator& orig) {
    data = orig.data;
    return *this;
}

char* my_iterator::operator ->() {
    return data;
}

char& my_iterator::operator *() {
    return data[0];
}

my_iterator& my_iterator::operator+=(int n) {
    data += n;
    return *this;
}

my_iterator& my_iterator::operator-=(int n) {
    return operator +=(-n);
}

my_iterator my_iterator::operator+(int n) {
    my_iterator temp(*this);
    temp += n;
    return temp;
}

my_iterator my_iterator::operator-(int n) {
    return operator +(-n);
}

char& my_iterator::operator[](int n) {
    return data[n];
}
