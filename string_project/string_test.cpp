
#define PRINTALL
#define TESTNAME __FUNCTION__
#define _TEST_START_ print_test_start(__FUNCTION__)


#include <iomanip>
#include <locale>
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <string.h>

#include "my_string.h"




using namespace std;

class test_fail_exception : exception {
public:

    test_fail_exception(const char* testRun, const my_string& res, const my_string& exp) throw () {
        int len;
        len = strlen(testRun);
        test = new char[len + 1];
        memcpy(test, testRun, len + 1);

        len = res.length();
        result = new char[len + 1];
        memcpy(result, res.c_str(), len + 1);

        len = exp.length();
        expected = new char[len + 1];
        memcpy(expected, exp.c_str(), len + 1);
    }

    void print_message() {
        cout << "FAIL in test: " << test << "()." << endl;
        cout << "Expected '" << expected << "', but got '" << result << "'." << endl;
    }

private:
    char* test;
    char* result;
    char* expected;
};


typedef void (test_function) ();

int testsRun = 0;
int fails = 0;
const char* curr_test;

void print_test_start(const char* test_name) {
    curr_test = test_name;
#ifdef PRINTALL
    cout << "TEST STARTED: " << test_name << "()" << endl;
#endif
}

void check_same(const my_string& result, const my_string& expected) {
    if (result != expected)
        throw test_fail_exception(curr_test, result, expected);
}

void check_same(int result, int expected) {
    ostringstream stream1, stream2;
    stream1 << result;
    stream2 << expected;
    my_string r = stream1.str().c_str();
    my_string e = stream2.str().c_str();
    check_same(r, e);
}

/*
 * This is used here to check that the memory address pointed to by the 
 * iterator is exactly the same as the one pointed to by the my_string 
 * object.
 */
void check_same_address(my_string::iterator it, const void* s) {
    /* 
     * With &(*it) you can actually access the private char* data in my_string!
     * This can also be done in std::string, which means there is a way to 
     * "destroy" the string object's invariance!
     */
    void* result = (void*) (&(*it));
    void* expected = (void*) s;
    if (result != expected) {
        ostringstream stream1, stream2;
        stream1 << result;
        stream2 << expected;
        my_string r = stream1.str().c_str();
        my_string e = stream2.str().c_str();
        check_same(r, e);
    }
}

/*
 * Checks that both iterators point to the same address.
 */
void check_same_address(my_string::iterator it1, my_string::iterator it2) {
    check_same_address(it1, &(*it2));
}

void run_test(test_function& test) {

    bool success = true;
    try {
        test();
    } catch (test_fail_exception e) {
        cout << "TEST FAILED!" << endl;
        e.print_message();
        success = false;
    } catch (const exception& exc) {
        cout << "TEST ENDED IN THE FOLLOWING EXCEPTION:" << endl;
        cout << exc.what() << endl;
        success = false;
    }

    testsRun++;
    if (!success)
        fails++;

#ifdef PRINTALL
    if (success)
        cout << "TEST SUCCESSFULLY ENDED!" << endl;
#endif

    cout << endl;

}

/*
 * 
 * 
 * TESTS start here!
 * 
 * 
 */



void default_constructor_test() {
    _TEST_START_;
    my_string s;
    check_same(s, "");
    check_same(s.length(), 0);
}

void char_constructor_test() {
    _TEST_START_;
    my_string s = 'a';
    check_same(s, "a");
}

void int_constructor_test() {
    _TEST_START_;
    my_string s(42);
    check_same(s.allocated_memory(), 43);
    check_same(s.length(), 0);
}

void stream_test1() {
    _TEST_START_;
    my_string ms;
    stringstream ss;
    ss << "Text";
    ss >> ms;
    check_same(ms, "Text");
}

void stream_test2() {
    _TEST_START_;
    my_string ms = "Something";
    stringstream ss;
    ss << "Text";
    ss >> ms;
    check_same(ms, "Text");
}

void stream_test3() {
    _TEST_START_;
    my_string ms = "Output";
    stringstream ss;
    ss << ms;
    check_same(ss.str().c_str(), "Output");
}

void erase_test_1() {
    _TEST_START_;
    my_string s = "FooFooFoo";
    my_string s2 = s.erase(0, 2);
    check_same(s, "oFooFoo");
    check_same(s2, "Fo");
}

void erase_test_2() {
    _TEST_START_;
    my_string s = "FooFooFoo";
    my_string s2 = s.erase(3, 1);
    check_same(s, "FooooFoo");
    check_same(s2, "F");
}

void erase_test_3() {
    _TEST_START_;
    my_string s = "FooFooFoo";
    my_string s2 = s.erase(6, 100);
    check_same(s, "FooFoo");
    check_same(s2, "Foo");
}

void erase_test_4() {
    _TEST_START_;
    my_string s = "University of helsinki";
    my_string s2 = s.erase(11, 6);
    check_same(s, "University sinki");
    check_same(s2, "of hel");
}

void push_back_test1() {
    _TEST_START_;
    int pushes = 23;
    my_string s;
    for (int i = 1; i <= pushes; i++) {
        s.push_back('a');
    }
}

void push_back_test2() {
    _TEST_START_;
    my_string s = "a";
    s.push_back('b');
    s.push_back('c');
    s.push_back('d');
    s.push_back('e');
    for (int i = 0; i < 300; i++) {
        s.push_back('r');
    }
    for (int i = 0; i < 300; i++) {
        s.pop_back();
    }
    check_same(s, "abcde");
}

void pop_back_test1() {
    _TEST_START_;
    my_string s = "abcdef";
    check_same('f', s.pop_back());
    check_same("abcde", s);
    check_same('e', s.pop_back());
    check_same("abcd", s);
    check_same('d', s.pop_back());
    check_same("abc", s);
}

void pop_back_test2() {
    _TEST_START_;
    my_string s(20);
    s += "abcdef";
    check_same('f', s.pop_back());
    check_same("abcde", s);
}

void oper_plusequals_test1() {
    _TEST_START_;
    my_string s = "C++";
    s += " course";
    check_same(s, "C++ course");
}

void oper_plusequals_test2() {
    _TEST_START_;
    my_string s;
    s += "Hello world!";
    check_same(s, "Hello world!");
}

void oper_plusequals_test3() {
    _TEST_START_;
    my_string s1;
    my_string s2 = "Hello world!";
    s1 += s2;
    check_same(s1, "Hello world!");
    check_same(s2, "Hello world!");
}

void oper_plusequals_test4() {
    _TEST_START_;
    my_string s1 = "Hello";
    my_string s2 = " world!";
    s1 += s2;
    check_same(s1, "Hello world!");
    check_same(s2, " world!");
}

void oper_plus_test1() {
    _TEST_START_;
    my_string s1;
    my_string s2 = "Jello";
    my_string s3 = s1 + s2;
    check_same(s1, "");
    check_same(s2, "Jello");
    check_same(s3, "Jello");
}

void oper_plus_test2() {
    _TEST_START_;
    my_string s1 = "Hello";
    my_string s2 = " world!";
    my_string s3 = s1 + s2;
    check_same(s1, "Hello");
    check_same(s2, " world!");
    check_same(s3, "Hello world!");
}

void oper_plus_test3() {
    _TEST_START_;
    my_string s1 = "Hello";
    my_string s2 = s1 + " world!";
    check_same(s1, "Hello");
    check_same(s2, "Hello world!");
}

void oper_plus_test4() {
    _TEST_START_;
    my_string s1 = "Hello";
    my_string s2 = s1 + '!';
    check_same(s1, "Hello");
    check_same(s2, "Hello!");
}

void oper_bracket_test1() {
    _TEST_START_;
    my_string s = "Foo";
    check_same(s[0], 'F');
}

void oper_bracket_test2() {
    _TEST_START_;
    my_string s = "Foo";
    s[0] = 'G';
    check_same(s, "Goo");
}

void oper_bracket_test3() {
    _TEST_START_;
    my_string s = "Hello";
    for (int i = 0; i < s.length() - 1; i++) {
        s[i + 1] = s[i];
    }
    check_same(s, "HHHHH");
}

void swap_test() {
    _TEST_START_;
    my_string s1 = "Hello";
    my_string s2 = "Zachary";
    s1.swap(s2);
    check_same(s1, "Zachary");
    check_same(s2, "Hello");
}

/*
 * 
 * Iterator tests follow.
 * 
 */


void iterator_copy_constructor_test() {
    _TEST_START_;
    my_string s = "Some kind of string...";
    my_string::iterator it = s.begin();
    my_string::iterator it2 = it;
    check_same_address(it, it2);
}

void iterator_assignment_test() {
    _TEST_START_;
    my_string s = "Some kind of string...";
    my_string::iterator it = s.begin();
    my_string::iterator it2;
    it2 = it;
    check_same_address(it, it2);
}

void iterator_begin_test() {
    _TEST_START_;
    my_string s = "Foo";
    my_string::iterator it = s.begin();
    check_same_address(it, s.c_str());
}

void iterator_end_test() {
    _TEST_START_;
    my_string s = "Foo";
    my_string::iterator it = s.end();
    check_same_address(it, s.c_str() + s.length());
}

void iterator_prefix_increment_test() {
    _TEST_START_;
    my_string s = "Foo";
    my_string::iterator it = s.begin();
    check_same_address(++it, s.c_str() + 1);
}

void iterator_postfix_increment_test() {
    _TEST_START_;
    my_string s = "Foo";
    my_string::iterator it = s.begin();
    check_same_address(it++, s.c_str());
    check_same_address(it, s.c_str() + 1);
}

void iterator_prefix_decrement_test() {
    _TEST_START_;
    my_string s = "Foo";
    my_string::iterator it = s.end();
    check_same_address(--it, s.c_str() + s.length() - 1);
}

void iterator_postfix_decrement_test() {
    _TEST_START_;
    my_string s = "Foo";
    my_string::iterator it = s.end();
    check_same_address(it--, s.c_str() + s.length());
    check_same_address(it, s.c_str() + s.length() - 1);
}

void iterator_plusequals_test() {
    _TEST_START_;
    my_string s = "This is a my_string!";
    my_string::iterator it = s.begin();
    it += 5;
    check_same_address(it, s.c_str() + 5);
}

void iterator_minusequals_test() {
    _TEST_START_;
    my_string s = "This is a my_string!";
    my_string::iterator it = s.end();
    it -= 5;
    check_same_address(it, s.c_str() + s.length() - 5);
}

void iterator_plus_test() {
    _TEST_START_;
    my_string s = "This is a my_string!";
    my_string::iterator it = s.begin();
    my_string::iterator it2 = it + 5;
    check_same_address(it, s.begin());
    check_same_address(it2, s.c_str() + 5);
}

void iterator_minus_test() {
    _TEST_START_;
    my_string s = "This is a my_string!";
    my_string::iterator it = s.end();
    my_string::iterator it2 = it - 5;
    check_same_address(it, s.end());
    check_same_address(it2, s.c_str() + s.length() - 5);
}

void iterator_bracket_test1() {
    _TEST_START_;
    my_string s = "This is a my_string!";
    my_string::iterator it = s.begin();
    check_same(it[4], s[4]);
}

void iterator_bracket_test2() {
    _TEST_START_;
    my_string s = "Foo";
    my_string::iterator it = s.begin();
    it[0] = 'W';
    check_same(s, "Woo");
}

/*
 * 
 * The main that calls all runs for the tests
 * 
 */
int main() {

    cout << endl;
    cout << "---------------------------------------" << endl;
    cout << "Starting tests for class my_string." << endl;
    cout << "---------------------------------------" << endl << endl;


    run_test(default_constructor_test);
    run_test(char_constructor_test);
    run_test(int_constructor_test);
    run_test(stream_test1);
    run_test(stream_test2);
    run_test(stream_test3);
    run_test(erase_test_1);
    run_test(erase_test_2);
    run_test(erase_test_3);
    run_test(erase_test_4);
    run_test(push_back_test1);
    run_test(push_back_test2);
    run_test(pop_back_test1);
    run_test(pop_back_test2);
    run_test(oper_plusequals_test1);
    run_test(oper_plusequals_test2);
    run_test(oper_plusequals_test3);
    run_test(oper_plusequals_test4);
    run_test(oper_plus_test1);
    run_test(oper_plus_test2);
    run_test(oper_plus_test3);
    run_test(oper_plus_test4);
    run_test(oper_bracket_test1);
    run_test(oper_bracket_test2);
    run_test(oper_bracket_test3);
    run_test(swap_test);

    // iterator tests
    run_test(iterator_copy_constructor_test);
    run_test(iterator_assignment_test);
    run_test(iterator_begin_test);
    run_test(iterator_end_test);
    run_test(iterator_prefix_increment_test);
    run_test(iterator_postfix_increment_test);
    run_test(iterator_prefix_decrement_test);
    run_test(iterator_postfix_decrement_test);
    run_test(iterator_plusequals_test);
    run_test(iterator_minusequals_test);
    run_test(iterator_plus_test);
    run_test(iterator_minus_test);
    run_test(iterator_bracket_test1);
    run_test(iterator_bracket_test2);


    cout << "---------------------------------------" << endl;
    cout << "All tests done!" << endl;
    cout << "Ran a total of " << testsRun << " tests, "
            << fails << " failed." << endl;
    cout << "---------------------------------------" << endl << endl;

    return (EXIT_SUCCESS);
}

