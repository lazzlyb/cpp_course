
#ifndef MY_ITERATOR_H
#define	MY_ITERATOR_H

class my_iterator {
public:
    
    my_iterator();
    my_iterator(const my_iterator& orig);
    my_iterator(char*);
    virtual ~my_iterator();
    
    // bidirectional iterator requirements
    bool operator==(const my_iterator&)const;
    bool operator!=(const my_iterator&)const;
    my_iterator& operator++();
    my_iterator operator++(int);
    my_iterator& operator--();
    my_iterator operator--(int);
    my_iterator& operator=(const my_iterator&);
    char* operator->();
    char& operator*();
    
    // random access iterator requirements
    my_iterator& operator+=(int);
    my_iterator& operator-=(int);
    my_iterator operator+(int);
    my_iterator operator-(int);
    char& operator[](int);
    
private:
    char* data;
};

#endif	/* MY_ITERATOR_H */

