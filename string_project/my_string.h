
#ifndef MY_STRING_H
#define	MY_STRING_H

#include <iostream>

#include "my_iterator.h"

class my_string {
public:
    my_string();
    my_string(const my_string&);
    my_string(const char);
    my_string(const char*);
    my_string(int);
    virtual ~my_string();
    bool operator==(const char) const;
    bool operator==(const char*) const;
    bool operator==(const my_string&) const;
    bool operator!=(const char) const;
    bool operator!=(const char*) const;
    bool operator!=(const my_string&) const;
    my_string& operator=(const char);
    my_string& operator=(const char*);
    my_string& operator=(const my_string&);
    my_string& operator+=(const char);
    my_string& operator+=(const char*);
    my_string& operator+=(const my_string&);
    my_string operator+(const my_string&) const;
    char& operator[](const unsigned int);
    unsigned int length() const;
    unsigned int allocated_memory() const;
    unsigned int free_space() const;
    friend std::ostream& operator<<(std::ostream&, const my_string&);
    friend std::istream& operator>>(std::istream&, my_string&);
    void push_back(const char c);
    char pop_back();
    my_string& insert(unsigned int i, const char);
    my_string& insert(unsigned int i, const char*);
    my_string& insert(unsigned int i, const my_string&);
    my_string& erase(unsigned int s_pos, unsigned int n);
    const char* c_str() const;
    void swap(my_string&);


    typedef my_iterator iterator;

    iterator begin();
    iterator end();

private:
    unsigned int data_size;
    unsigned int string_length;
    char* data;

    static const unsigned int BUFFER = 10;

    void expand(unsigned int);
    void add_to_data(const char*);
    void add_to_data(const char*, unsigned int);
    void check() const;

};

void copy_memory(void*, const void*, unsigned int);
unsigned int calc_string_length(const char*);

#endif	/* MY_STRING_H */

