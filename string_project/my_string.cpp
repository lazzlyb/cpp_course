
#include <stdexcept>
#include <stdio.h>

#include "my_string.h"

my_string::my_string() :
data_size(BUFFER), string_length(0), data(new char[data_size]) {
    data[0] = '\0';
    check();
}

my_string::my_string(const my_string& orig) :
data_size(orig.data_size),
string_length(orig.string_length),
data(new char[data_size]) {
    copy_memory(data, orig.data, string_length + 1);
    check();
}

my_string::my_string(const char c) :
data_size(2 + BUFFER), string_length(1), data(new char[data_size]) {
    data[0] = c;
    data[1] = '\0';
    check();
}

my_string::my_string(const char* str) :
data_size(calc_string_length(str) + BUFFER),
string_length(data_size - BUFFER),
data(new char[data_size]) {
    copy_memory(data, str, string_length + 1);
    check();
}

/*
 * Generates new my_string with (space + 1) memory. (+1 due to the 
 * terminating character at the end of the string, so that a (space) 
 * long string can fit into the my_string.)
 */
my_string::my_string(int space) {
    string_length = 0;
    if (space < 1) {
        data_size = 1;
    } else {
        data_size = (unsigned) space + 1;
    }
    data = new char[data_size];
    data[0] = '\0';
    check();
}

my_string::~my_string() {
    delete[] data;
}

void my_string::add_to_data(const char* str) {
    add_to_data(str, calc_string_length(str));
}

void my_string::add_to_data(const char* str, unsigned int len) {
    if (string_length + len >= data_size) {
        expand(len);
    }
    copy_memory(data + string_length, str, len);
    string_length += len;
    data[string_length] = '\0';
    check();
}

void my_string::expand(unsigned int len) {
    data_size += len + BUFFER;
    char* temp = new char[data_size];
    copy_memory(temp, data, string_length + 1);
    delete[] data;
    data = temp;
}

void copy_memory(void* dest, const void* src, unsigned int len) {
    char* d = (char*) dest;
    char* s = (char*) src;
    for (unsigned int i = 0; i < len; ++i) {
        *(d++) = *(s++);
    }
}

unsigned int calc_string_length(const char* str) {
    unsigned int len = 0;
    while (*(str++) != '\0') {
        ++len;
    }
    return len;
}

bool my_string::operator==(const char c) const {
    if (string_length != 1 || data[0] != c)
        return false;
    return true;
}

bool my_string::operator==(const char* str) const {
    if (string_length != calc_string_length(str))
        return false;
    for (int i = 0; i < string_length; i++) {
        if (data[i] != str[i])
            return false;
    }
    return true;
}

bool my_string::operator==(const my_string& str) const {
    return operator==(str.data);
}

bool my_string::operator!=(const char c) const {
    return !(operator==(c));
}

bool my_string::operator!=(const char* str) const {
    return !(operator==(str));
}

bool my_string::operator!=(const my_string& str) const {
    return !(operator==(str));
}

my_string& my_string::operator=(const char c) {
    delete[] data;
    data_size = 2 + BUFFER;
    string_length = 1;
    data = new char[data_size];
    data[0] = c;
    data[1] = '\0';
    check();
    return *this;
}

my_string& my_string::operator=(const char* str) {
    delete[] data;
    unsigned int len = calc_string_length(str);
    data = new char[len + BUFFER];
    copy_memory(data, str, len + 1);
    data_size = len + BUFFER;
    string_length = len;
    check();
    return *this;
}

my_string& my_string::operator=(const my_string& str) {
    delete[] data;
    data_size = str.data_size;
    string_length = str.string_length;
    data = new char[data_size];
    copy_memory(data, str.data, string_length + 1);
    check();
    return *this;
}

my_string& my_string::operator+=(const char c) {
    push_back(c);
    check();
    return *this;
}

my_string& my_string::operator+=(const char* str) {
    add_to_data(str);
    check();
    return *this;
}

my_string& my_string::operator+=(const my_string& str) {
    add_to_data(str.data);
    return *this;
}

my_string my_string::operator+(const my_string& str) const {
    my_string temp = *this;
    temp.add_to_data(str.data);
    return temp;
}

char& my_string::operator[](const unsigned int i) {
    return data[i];
}

std::ostream& operator<<(std::ostream& io, const my_string& str) {
    io << str.data;
    return io;
}

std::istream& operator>>(std::istream& io, my_string& str) {
    char temp[81];
    io.getline(temp, 80);
    str = temp;
    return io;
}

unsigned int my_string::length() const {
    return string_length;
}

unsigned int my_string::allocated_memory() const {
    return data_size;
}

unsigned int my_string::free_space() const {
    return data_size - string_length - 1;
}

void my_string::push_back(const char c) {
    char temp[2] = {c, '\0'};
    add_to_data(temp);
    check();
}

char my_string::pop_back() {
    if (string_length == 0)
        throw std::logic_error("Empty string!");
    char c = data[string_length - 1];
    erase(string_length - 1, 1);
    check();
    return c;
}

my_string& my_string::insert(unsigned int i, const my_string& str) {
    if (i > string_length)
        throw std::out_of_range("Index out of range!");
    insert(i, str.data);
    check();
    return *this;
}

my_string& my_string::insert(unsigned int i, const char c) {
    if (i > string_length)
        throw std::out_of_range("Index out of range!");
    char temp[2] = {c, '\0'};
    insert(i, temp);
    check();
    return *this;
}

my_string& my_string::insert(unsigned int i, const char* str) {
    if (i > string_length)
        throw std::out_of_range("Index out of range!");
    int rem_len = string_length - i;
    char remainder[rem_len];
    copy_memory(remainder, data + i, rem_len + 1);
    string_length = i;
    add_to_data(str);
    add_to_data(remainder);
    check();
    return *this;
}

my_string& my_string::erase(unsigned int s_pos, unsigned int n) {
    if (s_pos > string_length)
        throw std::out_of_range("Index out of range!");
    int erase_end = s_pos + n;
    my_string* erased = new my_string();
    if (erase_end > string_length) {
        erased->add_to_data(data + s_pos);
        string_length = s_pos;
        data[s_pos] = '\0';
    } else {
        erased->add_to_data(data + s_pos, n);
        char* temp = data + erase_end;
        string_length = s_pos;
        add_to_data(temp);
    }
    check();
    erased->check();
    return *erased;
}

const char* my_string::c_str() const {
    return data;
}

void my_string::swap(my_string& other) {
    char* temp_data = data;
    data = other.data;
    other.data = temp_data;

    unsigned int temp = string_length;
    string_length = other.string_length;
    other.string_length = temp;

    temp = data_size;
    data_size = other.data_size;
    other.data_size = temp;
}

my_string::iterator my_string::begin() {
    return my_string::iterator(data);
}

my_string::iterator my_string::end() {
    return my_string::iterator(data + string_length);
}

/*
 * Providing self-check method check().
 * 
 * 
 * If NDEBUG is defined the function body will be empty.
 * 
 * 
 * If NDEBUG is NOT defined, the check will proceed as follows:
 * 
 * Method will check the state of this instance of my_string, and throw a 
 * std::runtime_error if the state is invalid.
 * 
 * The invariance, which is checked is the following:
 * The char* data should contain chars other than '\0' from indeces 0 through
 * string_length - 1, and at index string_length it should contain the char '\0'.
 * Also, data_size should always be at least one greater than string_length (to 
 * fit the '\0'-char).
 * 
 * The check method also checks for possible other exceptions that might be thrown
 * trying to access the characters in data. This might happen if not enough memory
 * was allocated for the char-data.
 * 
 */

#ifndef NDEBUG
#include <iostream>
#endif

void my_string::check() const {

#ifndef NDEBUG
    //    std::cout << "check!" << std::endl;
    if (string_length >= data_size)
        throw std::runtime_error("string_length >= data_size");
    char c;
    for (int i = 0; i < string_length; i++) {
        try {
            c = data[i];
        } catch (std::exception exc) {
            std::cout << "ERROR:" << std::endl;
            std::cout << "Accessing index " << i <<
                    " in my_string throw the following exception:" << std::endl;
            throw exc;
        }
        if (c == '\0')
            throw std::runtime_error("Terminated too soon!");
    }
    if (data[string_length] != '\0')
        throw std::runtime_error("Not terminated!");
#endif

}

