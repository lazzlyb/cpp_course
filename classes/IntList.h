/* 
 * File:   IntList.h
 * Author: lasse
 *
 * Created on November 6, 2012, 12:07 PM
 */

#ifndef INTLIST_H
#define	INTLIST_H

class IntList {
public:
    IntList();
    IntList(const IntList& orig);
    virtual ~IntList();
    void add(int a);
    const int* getList();
    int size();
    int& IntList::operator[] (unsigned int i);
protected:
    int* items;
private:
    static const int SIZE = 10;
    int numOfItems;
    int arraySize;
    void expand();
};

#endif	/* INTLIST_H */

