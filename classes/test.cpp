/* 
 * File:   test.cpp
 * Author: lasse
 *
 * Created on November 6, 2012, 12:31 PM
 */

#include <cstdlib>
#include <iostream>
#include <time.h>

#include "IntList.h"

using namespace std;

void printList(IntList* list) {
    const int* a = list->getList();
    int len = list->size();
    for (int i = 0; i < len; i++) {
        std::cout << a[i] << " ";
    }
    std::cout << "\n";
}

/*
 * 
 */
int main(void) {
    IntList list;
    srand(time(NULL));
    for (int i = 0; i < 15; i++) {
        list.add(rand() % 10);
    }
    printList(&list);
    return 0;
}

