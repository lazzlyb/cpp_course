/* 
 * File:   string_test.cpp
 * Author: lasse
 *
 * Created on November 7, 2012, 3:21 PM
 */

#include <cstdlib>
#include <iostream>

#include "my_string.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    my_string s;
    s[0] = 'a';
    s[1] = '\0';
    cout << s;
    return 0;
}

