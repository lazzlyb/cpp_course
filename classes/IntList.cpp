/* 
 * File:   IntList.cpp
 * Author: lasse
 * 
 * Created on November 6, 2012, 12:07 PM
 */

#include <stdlib.h>

#include "IntList.h"

IntList::IntList() {
    items = (int*) malloc(SIZE * sizeof (int));
    arraySize = SIZE;
    numOfItems = 0;
}

IntList::IntList(const IntList& orig) {
}

IntList::~IntList() {
    free(items);
}

void IntList::add(int a) {
    if (arraySize == numOfItems)
        expand();
    items[numOfItems++] = a;
}

void IntList::expand() {
    arraySize *= 2;
    items = (int*) realloc(items, arraySize * sizeof (int));
}

const int* IntList::getList() {
    return items;
}

int IntList::size() {
    return numOfItems;
}

int& IntList::operator[] (unsigned int i){
    return items[i];
}

