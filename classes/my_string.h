/* 
 * File:   my_string.h
 * Author: lasse
 *
 * Created on November 7, 2012, 3:19 PM
 */

#ifndef MY_STRING_H
#define	MY_STRING_H

class my_string {
public:
    my_string();
    my_string(const my_string& orig);
    virtual ~my_string();

    char& operator[] (int i) {
        return str[i];
    }
    
    my_string& operator=(char* cptr){
        str = cptr;
    }
    
    std::ostream& operator<<(std::ostream& os, const my_string& str){
//        os << str.str;
        return os;
    }
private:
    char* str;
    int length;
    int allocated;
    static const int DEFAULT_SIZE = 20;
};

#endif	/* MY_STRING_H */

