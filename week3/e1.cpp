


#include <cstdlib>
#include <vector>
#include <ctime>
#include <iostream>
#include <string>

#include <assert.h>

using namespace std;

template<typename T>
vector<T> reverse(const vector<T>& vec) {
    int len = vec.size();
    vector<T> newV(len);
    for (int i = 0; i < len; i++) {
        newV[i] = vec[len - i - 1];
    }
    return newV;
}

template<typename T>
void reverseOverwrite(vector<T>& vec) {
    int len = vec.size();
    T temp;
    for (int i = 0; i < len / 2; i++) {
        temp = vec[i];
        vec[i] = vec[len - i - 1];
        vec[len - i - 1] = temp;
    }
}

template<typename T>
void printVector(const vector<T>& v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << " ";
    }
    cout << "\n";
}

int main() {
//    vector<int> a, b;
//    int size = 10;
//    srand((unsigned) time(0));
//
//    for (int i = 0; i < size; i++) {
//        a.push_back(rand() % 10);
//    }
//    printVector(a);
//
//    b = reverse(a);
//    printVector(b);
//
//    reverseOverwrite(a);
//    printVector(a);
//
//
//    vector<string> as, bs;
//    as.push_back("Lasse");
//    as.push_back("Toffe");
//    as.push_back("Berg");
//    as.push_back("Sebbe");
//    
//    assert(as[0] == "Lasse");
//    
//
//    printVector(as);
//    reverseOverwrite(as);
//    printVector(as);

    
    vector<int> a, b, c, d;
    a.push_back(1);
    a.push_back(2);
    b.push_back(5);
    b.push_back(7);
    
    c = reverse(a);
    d = reverse(b);
    
    printVector(c);
    printVector(d);
    
    return 0;
}
