/* 
 * File:   algoe6.cpp
 * Author: llybeck
 *
 * Created on 18 January 2013, 13:33
 */

#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

void printBinaryTerms(int a) {
    cout << "The binary terms in " << a << " are:" << endl;
    int t;
    while (a) {
        t = pow(2, (int) log2(a));
        cout << t << " ";
        a = a - t;
    }
    cout << endl;
}

void printBinaryTermsWithNegatives(int a) {
    cout << "The binary terms with negatives in " << a << " are:" << endl;
    int t, lg;
    while (a) {
        lg = (int) log2(a);
        t = pow(-2, lg);
        if (t < 0) {
            cout << (-2) * t << " ";
            a = a - (-2) * t;
        }
        cout << t << " ";
        a = a - t;
    }
    cout << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {

    cout << endl;

    printBinaryTerms(185);
    printBinaryTerms(123456);

    cout << endl;

    printBinaryTermsWithNegatives(185);
    printBinaryTermsWithNegatives(123456);

    cout << endl;

    return 0;
}

