/* 
 * File:   e4b.cpp
 * Author: lasse
 *
 * Created on November 6, 2012, 2:34 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

void reportBadInput() {
    cout << "Bad input!\n";
}

void reportBadInput(int i) {
    reportBadInput();
    cout << "Aborted calculation with result " << i << ".\n";
}

/*
 * 
 */
int main(int argc, char** argv) {
    string s;
    char c;
    double i, result;
    while (true) {
        cin.clear();
        cin >> s;
        if (s == "quit" || s == "exit") {
            cout << "Bye bye!\n";
            break;
        }
        if (s.length() != 1) {
            reportBadInput();
            continue;
        }
        c = s[0];
        switch (c) {
            case '+':
                result = 0;
                break;
            case '*':
                result = 1;
                break;
            case '-':
                if (!(cin >> result)) {
                    result = 0;
                }
                break;
            case '/':
                if (!(cin >> result)) {
                    result = 0;
                }
                break;
            default:
                reportBadInput();
                continue;
        }
        while (cin >> i) {
            switch (c) {
                case '+':
                    result += i;
                    break;
                case '*':
                    result *= i;
                    break;
                case '-':
                    result -= i;
                    break;
                case '/':
                    result /= i;
                    break;
            }
        }
        cin.clear();
        cin >> c;
        if (c == ';') {
            cout << result << "\n";
        } else {
            reportBadInput(result);
        }
    }
    return 0;
}

