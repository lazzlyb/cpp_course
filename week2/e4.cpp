/* 
 * File:   e4.cpp
 * Author: lasse
 *
 * Created on November 5, 2012, 3:38 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <math.h>

using namespace std;

bool isDoubleFormat(string s) {
    int points = 0;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == '.') {
            points++;
            if (points > 1)
                return false;
        } else if (!isdigit(s[i]))
            return false;
    }
    return true;
}

double toDouble(string s) {
    if (!isDoubleFormat(s)) {
        cout << "WARNING!! Found potentially invalid input! Result might be wrong!\n";
    }
    const char* ptr = s.c_str();
    return atof(ptr);
}

void printResult(double result) {
    cout << "result: " << result << "\n";
}

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "This is a calculator.\n";
    string s;
    char operand;
    bool initialized, print;
    double d, result;
    initialized = false;
    cout << "Operations:\n"
            << "+ addition\n"
            << "- subtraction\n"
            << "* multiplication\n"
            << "/ division\n"
            << "e exponentiation\n"
            << "'exit' or 'quit' closes the calculator\n";
    while (true) {
        if (!initialized) {
            cin >> s;
            if (s == "quit" || s == "exit")
                break;
            if (!initialized) {
                if (s.length() != 1) {
                    cout << "No operand found!\n";
                    continue;
                }
                operand = s.at(0);
                switch (operand) {
                    case '+':
                        result = 0;
                        break;
                    case '-':
                        cin >> s;
                        if (s.at(s.length() - 1) == ';') {
                            s.resize(s.length() - 1);
                            cout << s << "\n";
                            continue;
                        }
                        result = toDouble(s);
                        break;
                    case '*':
                        result = 1;
                        break;
                    case '/':
                        cin >> s;
                        if (s.at(s.length() - 1) == ';') {
                            s.resize(s.length() - 1);
                            cout << s << "\n";
                            continue;
                        }
                        result = toDouble(s);
                        break;
                    case 'e':
                        cin >> s;
                        if (s.at(s.length() - 1) == ';') {
                            s.resize(s.length() - 1);
                            cout << s << "\n";
                            continue;
                        }
                        result = toDouble(s);
                        break;
                    default:
                        cout << "No operand found!\n";
                        continue;
                }
                initialized = true;
                print = false;
                continue;
            }
        }
        cin >> s;
        if (s.at(s.length() - 1) == ';') {
            s.resize(s.length() - 1);
            if (s.length() == 0) {
                printResult(result);
                continue;
            }
            initialized = false;
            print = true;
        }
        d = toDouble(s);
        switch (operand) {
            case '+':
                result += d;
                break;
            case '-':
                result -= d;
                break;
            case '*':
                result *= d;
                break;
            case '/':
                result /= d;
                break;
            case 'e':
                result = pow(result, d);
                break;
        }
        if (print) {
            printResult(result);
        }
    }
    cout << "Bye bye!\n";
    return 0;
}

