/* 
 * File:   e1.cpp
 * Author: lasse
 *
 * Created on November 5, 2012, 3:22 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

/*
 * 
 */
int main() {
    string s;
    cout << "This is an echo program. 'exit' exits the program.\n";
    while (true) {
        cin >> s;
        if (s == "exit")
            break;
        cout << "You wrote:\n" << s << "\n";
    }
    cout << "Bye bye!\n";
    return 0;
}

