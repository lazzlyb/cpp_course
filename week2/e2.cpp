
#include <iostream>
#include <math.h>
#include <string>
#include <stdio.h>

using namespace std;

int isPalindrome(int i) {
    char str[20];
    int len;
    len = sprintf(str, "%d", i);
    for (int i = 0; i < len / 2; i++) {
        if (str[i] != str[len - i - 1]) {
            return 0;
        }
    }
    return 1;
}

int main() {
    int max;
    cout << "Calculating all primes <= MAX.\n";
    cout << "Input MAX: ";
    cin >> max;
    int numbers[max + 1];
    numbers[0] = numbers[1] = 0;
    for (int i = 2; i <= max; i++) {
        numbers[i] = 1;
    }
    int sq = (int) sqrt(max);
    for (int i = 2; i <= max; i++) {
        if (numbers[i]) {
            cout << i << " ";
            if (i < sq)
                for (int j = 2; i * j <= max; j++) {
                    numbers[i * j] = 0;
                }
        }
    }
    cout << "\ndone.\n";
    return 0;
}
