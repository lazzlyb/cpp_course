/* 
 * File:   list_main.cpp
 * Author: lasse
 *
 * Created on November 25, 2012, 10:56 PM
 */

#include <cstdlib>
#include <iostream>

#include "SList.h"

using namespace std;

/*
 * 
 */

int main() {
    SList list;
    list.push_front("world");
    list.push_front("Hello");
    list.print();
    cout << list.first()->getData() << endl;
    list.pop_front();
    list.print();


    SList list1;
    list1.push_front("1");
    list1.push_front("2");
    list1.push_front("3");
    list1.push_front("4");
    list1.push_front("5");
    list1.push_front("6");
    list1.push_front("7");

    
    cout << "Iterator thingy." << endl;
    for (SList::iterator it = list1.begin(); it != list1.end(); it++) {
        cout << it->getData() << " ";
    }
    cout << endl << endl;

    cout << "list1:" << endl;
    list1.print();
    cout << endl;


    SList list2(list1);
    cout << "list2:" << endl;
    list2.print();
    cout << endl;

    list2.pop_front();
    const Node* n = list2.first();
    Node* k = n->getNext()->getNext();

    k->setNext(k->getNext()->getNext());

    cout << "After removal:" << endl;

    cout << "list1:" << endl;
    list1.print();
    cout << endl;

    cout << "list2:" << endl;
    list2.print();
    cout << endl;
    
    
    cout << "Clearing list2:" << endl;
    list2.clear();
    list2.print();
    
}

