/* 
 * File:   SList.h
 * Author: lasse
 *
 * Created on November 26, 2012, 11:39 AM
 */

#ifndef SLIST_H
#define	SLIST_H

#include <string>

class Node;
class SList;

class Node {
public:
    Node(Node* nxt, const std::string& str);
    Node(const Node& orig);
    virtual ~Node();
    Node* getNext() const;
    void setNext(Node* node);
    const std::string& getData() const;
private:
    Node* next;
    std::string data;
};

class SList {

private:
    
    class SList_iterator {
    public:
        SList_iterator(Node*);
        virtual ~SList_iterator();
        bool operator==(SList_iterator it);
        bool operator!=(SList_iterator it);
        Node* operator*();
        Node* operator->();

        friend SList_iterator& operator++(SList_iterator& it);
        friend SList_iterator operator++(SList_iterator& it, int);
    private:
        Node* current;
    };
    
public:

    typedef SList_iterator iterator;

    SList();
    SList(const SList& orig);
    virtual ~SList();
    Node const * first() const;
    void push_front(std::string);
    std::string pop_front();
    void print() const;
    void clear();
    SList& operator=(const SList& list);

    iterator begin();
    const iterator end();
    
private:
    
    Node* root;
    const iterator beyond_last;
    
};

#endif	/* SLIST_H */

