/* 
 * File:   SList.cpp
 * Author: lasse
 * 
 * Created on November 26, 2012, 11:40 AM
 */

#include <iostream>
#include <stdexcept>

#include "SList.h"

/*
 * 
 * Node class
 * 
 */

Node::Node(Node* nxt = NULL, const std::string& str = "") :
next(nxt), data(str) {
}

Node::Node(const Node& orig) {
    data = orig.data;
    if (orig.next == NULL)
        next = NULL;
    else
        next = new Node(*orig.next);
}

Node::~Node() {
}

Node* Node::getNext() const {
    return next;
}

void Node::setNext(Node* node) {
    next = node;
}

const std::string& Node::getData() const {
    return data;
}

/*
 * 
 * List class
 * 
 */

SList::SList() : root(NULL), beyond_last(new Node()) {
}

SList::SList(const SList& orig) : beyond_last(new Node()) {
    if (orig.root == NULL)
        root = NULL;
    else {
        root = new Node(*orig.root);
    }
}

SList::~SList() {
    Node* next;
    Node* node = root;
    while (node != NULL) {
        next = node->getNext();
        delete node;
        node = next;
    }
}

Node const * SList::first() const {
    return root;
}

void SList::push_front(std::string str) {
    root = new Node(root, str);
}

std::string SList::pop_front() {
    if (root == NULL)
        return NULL;
    std::string data = root->getData();
    Node* oldRoot = root;
    root = root->getNext();
    delete oldRoot;
    return data;
}

void SList::print() const {
    if (root == NULL)
        std::cout << "*EMPTY*" << std::endl;
    Node* node = root;
    while (node != NULL) {
        std::cout << node->getData() << " ";
        node = node->getNext();
    }
    std::cout << std::endl;
}

void SList::clear() {
    Node* temp;
    while (root != NULL) {
        temp = root->getNext();
        root->~Node();
        root = temp;
    }
}

SList& SList::operator=(const SList& list) {
    clear();
    if (list.root == NULL)
        root = NULL;
    else {
        root = new Node(*list.root);
    }
}

SList::SList_iterator::SList_iterator(Node* node) : current(node) {
}

SList::SList_iterator::~SList_iterator() {
}

SList::iterator SList::begin() {
    return root;
}

const SList::iterator SList::end() {
    return SList::SList_iterator(NULL);
}

SList::iterator& operator++(SList::iterator& it) {
    if (it.current == NULL)
        throw std::out_of_range("");
    it.current = it.current->getNext();
    return it;
}

SList::iterator operator++(SList::iterator& it, int) {
    SList::iterator temp = it;
    it = ++it;
    return temp;
}

bool SList::SList_iterator::operator ==(SList::SList_iterator it) {
    return current == it.current;
}

bool SList::SList_iterator::operator !=(SList::SList_iterator it) {
    return !(*this == it);
}

Node* SList::SList_iterator::operator *() {
    return current;
}

Node* SList::SList_iterator::operator ->() {
    return current;
}
