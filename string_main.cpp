/* 
 * File:   string_main.cpp
 * Author: Christoffer
 *
 * Created on December 4, 2012, 7:08 PM
 */

#include <cstdlib>
#include <iosfwd>
#include <string>
#include <iostream>
#include <vector>

#include <time.h>
#include <ctime>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    time_t start = time(NULL);
    clock_t st = clock();

    string s = "Toffe";
    string::iterator it = s.begin();
    it[0] = 'w';
    cout << s << endl;

    char* cptr = &(*it);
    cout << cptr << endl;
    cptr[1] = '\0';
    cout << cptr << endl;
    cout << s.length() << endl;
    cout << s << endl;
    cout << s.c_str() << endl;
    cout << (void*) (s.c_str()) << endl;
    cout << (void*) cptr << endl;

    cout << endl;
    vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);

    vector<int>::iterator i = v.begin();
    *i = 8;
    cout << i[0] << endl;


    int k = 0;
    for (int j = 0; j < 7000; j++) {
        cout << k << endl;
        k += 3;
    }


    double end = ((double) clock() - st) / CLOCKS_PER_SEC;

    cout << (time(NULL) - start) << " s" << endl;
    cout << end * 1000 << " ms = " << end << " s." << endl;

    return 0;
}

