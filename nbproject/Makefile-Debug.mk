#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/string_project/string_test.o \
	${OBJECTDIR}/week2/e1.o \
	${OBJECTDIR}/week4/e4.o \
	${OBJECTDIR}/classes/string_test.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/week5/list_main.o \
	${OBJECTDIR}/List.o \
	${OBJECTDIR}/week2/e4.o \
	${OBJECTDIR}/newmain.o \
	${OBJECTDIR}/toffe.o \
	${OBJECTDIR}/gsl_test.o \
	${OBJECTDIR}/week3/e1.o \
	${OBJECTDIR}/classes/IntList.o \
	${OBJECTDIR}/week2/e2.o \
	${OBJECTDIR}/week4/my_complex.o \
	${OBJECTDIR}/classes/my_string.o \
	${OBJECTDIR}/string_project/my_string.o \
	${OBJECTDIR}/classes/test.o \
	${OBJECTDIR}/week4/IntStack.o \
	${OBJECTDIR}/toffe2.o \
	${OBJECTDIR}/week3/algoe6.o \
	${OBJECTDIR}/week4/e2.o \
	${OBJECTDIR}/week5/SList.o \
	${OBJECTDIR}/week2/e4b.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cpp_course

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cpp_course: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cpp_course ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/string_project/string_test.o: string_project/string_test.cpp 
	${MKDIR} -p ${OBJECTDIR}/string_project
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/string_project/string_test.o string_project/string_test.cpp

${OBJECTDIR}/week2/e1.o: week2/e1.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e1.o week2/e1.cpp

${OBJECTDIR}/week4/e4.o: week4/e4.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/e4.o week4/e4.cpp

${OBJECTDIR}/classes/string_test.o: classes/string_test.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/string_test.o classes/string_test.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/week5/list_main.o: week5/list_main.cpp 
	${MKDIR} -p ${OBJECTDIR}/week5
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week5/list_main.o week5/list_main.cpp

${OBJECTDIR}/List.o: List.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/List.o List.cpp

${OBJECTDIR}/week2/e4.o: week2/e4.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e4.o week2/e4.cpp

${OBJECTDIR}/newmain.o: newmain.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmain.o newmain.cpp

${OBJECTDIR}/toffe.o: toffe.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/toffe.o toffe.cpp

${OBJECTDIR}/gsl_test.o: gsl_test.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/gsl_test.o gsl_test.c

${OBJECTDIR}/week3/e1.o: week3/e1.cpp 
	${MKDIR} -p ${OBJECTDIR}/week3
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week3/e1.o week3/e1.cpp

${OBJECTDIR}/classes/IntList.o: classes/IntList.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/IntList.o classes/IntList.cpp

${OBJECTDIR}/week2/e2.o: week2/e2.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e2.o week2/e2.cpp

${OBJECTDIR}/week4/my_complex.o: week4/my_complex.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/my_complex.o week4/my_complex.cpp

${OBJECTDIR}/classes/my_string.o: classes/my_string.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/my_string.o classes/my_string.cpp

${OBJECTDIR}/string_project/my_string.o: string_project/my_string.cpp 
	${MKDIR} -p ${OBJECTDIR}/string_project
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/string_project/my_string.o string_project/my_string.cpp

${OBJECTDIR}/classes/test.o: classes/test.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/test.o classes/test.cpp

${OBJECTDIR}/week4/IntStack.o: week4/IntStack.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/IntStack.o week4/IntStack.cpp

${OBJECTDIR}/toffe2.o: toffe2.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/toffe2.o toffe2.cpp

${OBJECTDIR}/week3/algoe6.o: week3/algoe6.cpp 
	${MKDIR} -p ${OBJECTDIR}/week3
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week3/algoe6.o week3/algoe6.cpp

${OBJECTDIR}/week4/e2.o: week4/e2.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/e2.o week4/e2.cpp

${OBJECTDIR}/week5/SList.o: week5/SList.cpp 
	${MKDIR} -p ${OBJECTDIR}/week5
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week5/SList.o week5/SList.cpp

${OBJECTDIR}/week2/e4b.o: week2/e4b.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e4b.o week2/e4b.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-conf ${TESTFILES}
${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/newsimpletest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS} 


${TESTDIR}/tests/newsimpletest.o: tests/newsimpletest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -I. -MMD -MP -MF $@.d -o ${TESTDIR}/tests/newsimpletest.o tests/newsimpletest.cpp


${OBJECTDIR}/string_project/string_test_nomain.o: ${OBJECTDIR}/string_project/string_test.o string_project/string_test.cpp 
	${MKDIR} -p ${OBJECTDIR}/string_project
	@NMOUTPUT=`${NM} ${OBJECTDIR}/string_project/string_test.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/string_project/string_test_nomain.o string_project/string_test.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/string_project/string_test.o ${OBJECTDIR}/string_project/string_test_nomain.o;\
	fi

${OBJECTDIR}/week2/e1_nomain.o: ${OBJECTDIR}/week2/e1.o week2/e1.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week2/e1.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e1_nomain.o week2/e1.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week2/e1.o ${OBJECTDIR}/week2/e1_nomain.o;\
	fi

${OBJECTDIR}/week4/e4_nomain.o: ${OBJECTDIR}/week4/e4.o week4/e4.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week4/e4.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/e4_nomain.o week4/e4.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week4/e4.o ${OBJECTDIR}/week4/e4_nomain.o;\
	fi

${OBJECTDIR}/classes/string_test_nomain.o: ${OBJECTDIR}/classes/string_test.o classes/string_test.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	@NMOUTPUT=`${NM} ${OBJECTDIR}/classes/string_test.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/string_test_nomain.o classes/string_test.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/classes/string_test.o ${OBJECTDIR}/classes/string_test_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

${OBJECTDIR}/week5/list_main_nomain.o: ${OBJECTDIR}/week5/list_main.o week5/list_main.cpp 
	${MKDIR} -p ${OBJECTDIR}/week5
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week5/list_main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week5/list_main_nomain.o week5/list_main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week5/list_main.o ${OBJECTDIR}/week5/list_main_nomain.o;\
	fi

${OBJECTDIR}/List_nomain.o: ${OBJECTDIR}/List.o List.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/List.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/List_nomain.o List.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/List.o ${OBJECTDIR}/List_nomain.o;\
	fi

${OBJECTDIR}/week2/e4_nomain.o: ${OBJECTDIR}/week2/e4.o week2/e4.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week2/e4.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e4_nomain.o week2/e4.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week2/e4.o ${OBJECTDIR}/week2/e4_nomain.o;\
	fi

${OBJECTDIR}/newmain_nomain.o: ${OBJECTDIR}/newmain.o newmain.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/newmain.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/newmain_nomain.o newmain.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/newmain.o ${OBJECTDIR}/newmain_nomain.o;\
	fi

${OBJECTDIR}/toffe_nomain.o: ${OBJECTDIR}/toffe.o toffe.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/toffe.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/toffe_nomain.o toffe.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/toffe.o ${OBJECTDIR}/toffe_nomain.o;\
	fi

${OBJECTDIR}/gsl_test_nomain.o: ${OBJECTDIR}/gsl_test.o gsl_test.c 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/gsl_test.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/gsl_test_nomain.o gsl_test.c;\
	else  \
	    ${CP} ${OBJECTDIR}/gsl_test.o ${OBJECTDIR}/gsl_test_nomain.o;\
	fi

${OBJECTDIR}/week3/e1_nomain.o: ${OBJECTDIR}/week3/e1.o week3/e1.cpp 
	${MKDIR} -p ${OBJECTDIR}/week3
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week3/e1.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week3/e1_nomain.o week3/e1.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week3/e1.o ${OBJECTDIR}/week3/e1_nomain.o;\
	fi

${OBJECTDIR}/classes/IntList_nomain.o: ${OBJECTDIR}/classes/IntList.o classes/IntList.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	@NMOUTPUT=`${NM} ${OBJECTDIR}/classes/IntList.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/IntList_nomain.o classes/IntList.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/classes/IntList.o ${OBJECTDIR}/classes/IntList_nomain.o;\
	fi

${OBJECTDIR}/week2/e2_nomain.o: ${OBJECTDIR}/week2/e2.o week2/e2.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week2/e2.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e2_nomain.o week2/e2.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week2/e2.o ${OBJECTDIR}/week2/e2_nomain.o;\
	fi

${OBJECTDIR}/week4/my_complex_nomain.o: ${OBJECTDIR}/week4/my_complex.o week4/my_complex.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week4/my_complex.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/my_complex_nomain.o week4/my_complex.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week4/my_complex.o ${OBJECTDIR}/week4/my_complex_nomain.o;\
	fi

${OBJECTDIR}/classes/my_string_nomain.o: ${OBJECTDIR}/classes/my_string.o classes/my_string.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	@NMOUTPUT=`${NM} ${OBJECTDIR}/classes/my_string.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/my_string_nomain.o classes/my_string.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/classes/my_string.o ${OBJECTDIR}/classes/my_string_nomain.o;\
	fi

${OBJECTDIR}/string_project/my_string_nomain.o: ${OBJECTDIR}/string_project/my_string.o string_project/my_string.cpp 
	${MKDIR} -p ${OBJECTDIR}/string_project
	@NMOUTPUT=`${NM} ${OBJECTDIR}/string_project/my_string.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/string_project/my_string_nomain.o string_project/my_string.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/string_project/my_string.o ${OBJECTDIR}/string_project/my_string_nomain.o;\
	fi

${OBJECTDIR}/classes/test_nomain.o: ${OBJECTDIR}/classes/test.o classes/test.cpp 
	${MKDIR} -p ${OBJECTDIR}/classes
	@NMOUTPUT=`${NM} ${OBJECTDIR}/classes/test.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/classes/test_nomain.o classes/test.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/classes/test.o ${OBJECTDIR}/classes/test_nomain.o;\
	fi

${OBJECTDIR}/week4/IntStack_nomain.o: ${OBJECTDIR}/week4/IntStack.o week4/IntStack.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week4/IntStack.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/IntStack_nomain.o week4/IntStack.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week4/IntStack.o ${OBJECTDIR}/week4/IntStack_nomain.o;\
	fi

${OBJECTDIR}/toffe2_nomain.o: ${OBJECTDIR}/toffe2.o toffe2.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/toffe2.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/toffe2_nomain.o toffe2.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/toffe2.o ${OBJECTDIR}/toffe2_nomain.o;\
	fi

${OBJECTDIR}/week3/algoe6_nomain.o: ${OBJECTDIR}/week3/algoe6.o week3/algoe6.cpp 
	${MKDIR} -p ${OBJECTDIR}/week3
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week3/algoe6.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week3/algoe6_nomain.o week3/algoe6.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week3/algoe6.o ${OBJECTDIR}/week3/algoe6_nomain.o;\
	fi

${OBJECTDIR}/week4/e2_nomain.o: ${OBJECTDIR}/week4/e2.o week4/e2.cpp 
	${MKDIR} -p ${OBJECTDIR}/week4
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week4/e2.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week4/e2_nomain.o week4/e2.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week4/e2.o ${OBJECTDIR}/week4/e2_nomain.o;\
	fi

${OBJECTDIR}/week5/SList_nomain.o: ${OBJECTDIR}/week5/SList.o week5/SList.cpp 
	${MKDIR} -p ${OBJECTDIR}/week5
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week5/SList.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week5/SList_nomain.o week5/SList.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week5/SList.o ${OBJECTDIR}/week5/SList_nomain.o;\
	fi

${OBJECTDIR}/week2/e4b_nomain.o: ${OBJECTDIR}/week2/e4b.o week2/e4b.cpp 
	${MKDIR} -p ${OBJECTDIR}/week2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/week2/e4b.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/week2/e4b_nomain.o week2/e4b.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/week2/e4b.o ${OBJECTDIR}/week2/e4b_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cpp_course

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
